//
//  ContentView.swift
//  SampleTwitterLogin
//
//  Created by Etsushi Otani on 2021/08/24.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var viewModel = ViewModel()
    
    var body: some View {
        VStack {
            Spacer()
            Button(action: {
                viewModel.login()
            }, label: {
                HStack {
                    Image("twitter")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 32, height: 32, alignment: .center)
                    Text("Sign in with Twitter")
                        .foregroundColor(.black)
                }
                .padding()
                .frame(maxWidth: .infinity)
            })
        }
        .padding(.horizontal)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
