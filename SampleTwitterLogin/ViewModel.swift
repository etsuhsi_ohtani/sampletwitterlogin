//
//  ViewModel.swift
//  SampleTwitterLogin
//
//  Created by Etsushi Otani on 2021/08/24.
//

import Foundation
import Firebase

class ViewModel: ObservableObject {
    
    private var provider: OAuthProvider?
    
    func login() {
        provider = OAuthProvider(providerID: "twitter.com")
        provider?.getCredentialWith(nil) { credential, error in
          if error != nil {
            // Handle error.
            print(error?.localizedDescription)
          }
          if let credential = credential {
            Auth.auth().signIn(with: credential) { authResult, error in
              if error != nil {
                // Handle error.
                print(error?.localizedDescription)
              }
              // User is signed in.
              // IdP data available in authResult.additionalUserInfo.profile.
              // Twitter OAuth access token can also be retrieved by:
              // authResult.credential.accessToken
              // Twitter OAuth ID token can be retrieved by calling:
              // authResult.credential.idToken
              // Twitter OAuth secret can be retrieved by calling:
              // authResult.credential.secret
                print(authResult?.credential)
            }
          }
        }
    }
}
